import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class BookFormat {
  @Field((_type) => ID)
  id: number;

  @Field()
  format: string;
}