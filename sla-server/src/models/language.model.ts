import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class Language {
  @Field((_type) => ID)
  id: number;

  @Field()
  language: string;
}