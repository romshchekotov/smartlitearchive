import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class BookCategory {
  @Field((_type) => ID)
  id: string;

  @Field()
  category: string;

  @Field({ nullable: true })
  image?: string;
}