import { Field, ID, ObjectType } from '@nestjs/graphql';
import { CreatorAlias } from './creator-alias.model';

@ObjectType()
export class Creator {
  @Field((_type) => ID)
  id: string;

  @Field()
  firstName: string;

  @Field()
  lastName: string;

  @Field({ nullable: true })
  image?: string;

  @Field({ nullable: true })
  age?: number;

  @Field({ nullable: true })
  nationality?: string;

  @Field((_type) => [CreatorAlias], { nullable: true })
  aliases?: CreatorAlias[];
}