import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class BookStatus {
  @Field((_type) => ID)
  id: number;

  @Field()
  status: string;
}