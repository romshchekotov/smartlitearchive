import { Field, ID, Int, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class BookSize {
  @Field((_type) => ID)
  id: string;

  @Field((_type) => Int)
  pages: number;

  @Field((_type) => Int)
  chapters: number;

  @Field((_type) => Int)
  volumes: number;
}