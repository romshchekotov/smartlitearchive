import { Field, ID, Int, ObjectType } from '@nestjs/graphql';
import { BookCategory } from './book-category.model';
import { BookData } from './book-data.model';
import { BookGenre } from './book-genre.model';
import { BookSize } from './book-size.model';
import { BookStatus } from './book-status.model';
import { Creator } from './creater.model';
import { Language } from './language.model';

@ObjectType()
export class Book {
  @Field((_type) => ID)
  id: string;

  @Field()
  title: string;

  @Field({ nullable: true })
  coverUrl?: string;

  @Field((_type) => [Creator])
  authors: Creator[];

  @Field((_type) => [Creator])
  artists: Creator[];

  @Field({ nullable: true })
  isbn?: string;

  @Field((_type) => [BookGenre])
  genres: BookGenre[];

  @Field((_type) => [BookCategory])
  categories: BookCategory[];

  @Field((_type) => Int)
  year: number;

  @Field((_type) => Language)
  language: Language;

  @Field((_type) => BookStatus)
  status: BookStatus;

  @Field((_type) => BookSize)
  size: BookSize;

  @Field((_type) => BookData)
  data: BookData;
}