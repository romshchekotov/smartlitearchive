import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class BookGenre {
  @Field((_type) => ID)
  id: string;

  @Field()
  genre: string;

  @Field({ nullable: true })
  image?: string;
}