import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class CreatorAlias {
  @Field((_type) => ID)
  id: number;

  @Field()
  name: string;
}