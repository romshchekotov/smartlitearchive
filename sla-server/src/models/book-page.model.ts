import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class BookPage {
  @Field((_type) => ID)
  id: string;

  @Field()
  file: string;
}