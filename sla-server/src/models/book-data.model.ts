import { Field, ID, ObjectType } from '@nestjs/graphql';
import { BookFormat } from './book-format.model';
import { BookPage } from './book-page.model';

@ObjectType()
export class BookData {
  @Field((_type) => ID)
  id: string;

  @Field((_type) => BookFormat, { nullable: true })
  format?: BookFormat;

  @Field((_type) => [BookPage], { nullable: true })
  files?: BookPage[];
}