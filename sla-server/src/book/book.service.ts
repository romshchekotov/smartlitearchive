import { Injectable } from '@nestjs/common';
import { prisma } from 'src/main';
import { Book } from 'src/models/book.model';

@Injectable()
export class BookService {
  async findAll(): Promise<Book[]> {
    return await prisma.book.findMany({
      include: {
        authors: true,
        artists: true,
        genres: true,
        categories: true,
        language: true,
        status: true,
        size: true,
        data: true,
      }
    });
  }
}
