import { Query, Resolver } from '@nestjs/graphql';
import { Book } from 'src/models/book.model';
import { BookService } from './book.service';

@Resolver((_of) => Book)
export class BookResolver {
  constructor(private readonly bookService: BookService) { }

  @Query((_returns) => [Book])
  async books(): Promise<Book[]> {
    return await this.bookService.findAll();
  }
}
