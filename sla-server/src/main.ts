import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { PrismaClient } from '@prisma/client';

let prisma: PrismaClient;

async function bootstrap() {
  prisma = new PrismaClient();
  const app = await NestFactory.create(AppModule);
  await app.listen(3000);
}
bootstrap();

export { prisma };