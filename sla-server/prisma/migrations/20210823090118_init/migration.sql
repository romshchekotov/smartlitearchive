-- CreateTable
CREATE TABLE "Book" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "coverUrl" TEXT,
    "title" TEXT NOT NULL,
    "isbn" TEXT,
    "year" INTEGER NOT NULL,
    "languageId" INTEGER NOT NULL,
    "statusId" INTEGER NOT NULL,
    "sizeId" TEXT NOT NULL,
    "dataId" TEXT NOT NULL,
    FOREIGN KEY ("languageId") REFERENCES "Language" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY ("statusId") REFERENCES "BookStatus" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY ("sizeId") REFERENCES "BookSize" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY ("dataId") REFERENCES "BookData" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);

-- CreateTable
CREATE TABLE "Creator" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "firstName" TEXT NOT NULL,
    "lastName" TEXT NOT NULL,
    "image" TEXT,
    "age" INTEGER,
    "nationality" TEXT
);

-- CreateTable
CREATE TABLE "CreatorAlias" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "name" TEXT NOT NULL,
    "creatorId" TEXT NOT NULL,
    FOREIGN KEY ("creatorId") REFERENCES "Creator" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);

-- CreateTable
CREATE TABLE "BookGenre" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "genre" TEXT NOT NULL,
    "image" TEXT
);

-- CreateTable
CREATE TABLE "BookCategory" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "category" TEXT NOT NULL,
    "image" TEXT
);

-- CreateTable
CREATE TABLE "Language" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "language" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "BookStatus" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "status" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "BookFormat" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "format" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "BookData" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "formatId" INTEGER NOT NULL,
    FOREIGN KEY ("formatId") REFERENCES "BookFormat" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);

-- CreateTable
CREATE TABLE "BookPage" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "file" TEXT NOT NULL,
    "dataId" TEXT NOT NULL,
    FOREIGN KEY ("dataId") REFERENCES "BookData" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);

-- CreateTable
CREATE TABLE "BookSize" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "pages" INTEGER NOT NULL,
    "chapters" INTEGER NOT NULL,
    "volumes" INTEGER NOT NULL,
    "bookId" TEXT
);

-- CreateTable
CREATE TABLE "_authored" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL,
    FOREIGN KEY ("A") REFERENCES "Book" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY ("B") REFERENCES "Creator" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);

-- CreateTable
CREATE TABLE "_illustrated" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL,
    FOREIGN KEY ("A") REFERENCES "Book" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY ("B") REFERENCES "Creator" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);

-- CreateTable
CREATE TABLE "_BookToBookGenre" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL,
    FOREIGN KEY ("A") REFERENCES "Book" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY ("B") REFERENCES "BookGenre" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);

-- CreateTable
CREATE TABLE "_BookToBookCategory" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL,
    FOREIGN KEY ("A") REFERENCES "Book" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY ("B") REFERENCES "BookCategory" ("id") ON DELETE CASCADE ON UPDATE CASCADE
);

-- CreateIndex
CREATE UNIQUE INDEX "Book_sizeId_unique" ON "Book"("sizeId");

-- CreateIndex
CREATE UNIQUE INDEX "_authored_AB_unique" ON "_authored"("A", "B");

-- CreateIndex
CREATE INDEX "_authored_B_index" ON "_authored"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_illustrated_AB_unique" ON "_illustrated"("A", "B");

-- CreateIndex
CREATE INDEX "_illustrated_B_index" ON "_illustrated"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_BookToBookGenre_AB_unique" ON "_BookToBookGenre"("A", "B");

-- CreateIndex
CREATE INDEX "_BookToBookGenre_B_index" ON "_BookToBookGenre"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_BookToBookCategory_AB_unique" ON "_BookToBookCategory"("A", "B");

-- CreateIndex
CREATE INDEX "_BookToBookCategory_B_index" ON "_BookToBookCategory"("B");
