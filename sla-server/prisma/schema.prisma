datasource db {
  provider = "sqlite"
  url      = "file:./database.db"
}

generator client {
  provider = "prisma-client-js"
}

model Book {
  id String @id @default(uuid())
  coverUrl String?
  title String
  authors Creator[] @relation("authored")
  artists Creator[] @relation("illustrated")
  isbn String?
  genres BookGenre[]
  year Int
  languageId Int
  language Language @relation(fields: [languageId], references: [id])
  categories BookCategory[]
  statusId Int
  status BookStatus @relation(fields: [statusId], references: [id])
  sizeId String
  size BookSize @relation(fields: [sizeId], references: [id])
  dataId String
  data BookData @relation(fields: [dataId], references: [id])
}

model Creator {
  id String @id @default(uuid())
  firstName String
  lastName String
  image String?
  age Int?
  nationality String?
  
  aliases CreatorAlias[]
  authored Book[] @relation("authored")
  illustrated Book[] @relation("illustrated")
}

model CreatorAlias {
  id Int @id @default(autoincrement())
  name String

  creatorId String
  creator Creator @relation(fields: [creatorId], references: [id])
}

model BookGenre {
  id String @id @default(uuid())
  genre String
  image String?

  books Book[]
}

model BookCategory {
  id String @id @default(uuid())
  category String
  image String?

  books Book[]
}

model Language {
  id Int @id @default(autoincrement())
  language String
  
  books Book[]
}

model BookStatus {
  id Int @id @default(autoincrement())
  status String
  
  books Book[]
}

model BookFormat {
  id Int @id @default(autoincrement())
  format String

  bookData BookData[]
}

model BookData {
  id String @id @default(uuid())

  formatId Int
  format BookFormat @relation(fields: [formatId], references: [id])

  files BookPage[]
  book Book?
}

model BookPage {
  id String @id @default(uuid())
  file String

  dataId String
  data BookData @relation(fields: [dataId], references: [id])
}

model BookSize {
  id String @id @default(uuid())

  pages Int
  chapters Int
  volumes Int

  book Book?
}